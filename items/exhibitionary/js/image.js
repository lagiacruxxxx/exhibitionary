

$(document).ready(function()
{	
	filemanagerListeners();
	igniteImagemanager();
	toggleImageListeners(true);
});

function igniteImagemanager()
{
	$('#filemanager').button();
	$('#image_container').sortable(
	{
		handle: '.move',
	});
	//$('#image_container').disableSelection();
}

function filemanagerListeners()
{
	$('#filemanager').on('click', function()
	{
		openFilemanager();
	});
	
	$('.save').on('click', function()
	{
		savePhotos();
	});
	
	$('.goback').on('click', function()
	{
		window.location.href = rootUrl + 'exhibition';
	});
}


function openFilemanager()
{
	$('#filemanPanel iframe').attr('src', rootUrl + 'items/exhibitionary/fileman/index.html');
	$('#filemanPanel').dialog({
		width: parseInt($(window).width() * 0.8),
		height: parseInt($(window).height() * 0.8),
		modal:true, 
	});
}

function closeFilemanager()
{
	$('#filemanPanel').dialog('destroy');
}

function addImage(imageData)
{
	var clone = $('.image.isTemplate').clone();
	clone.removeClass('isTemplate');
	clone.find('.imagefile').attr('src', imageData.fullPath);
	$('#image_container').append(clone);
	toggleImageListeners(false);
	toggleImageListeners(true);
}


function toggleImageListeners(toggle)
{
	if(toggle)
	{
		$('#image_container .image .delete').on('click', function()
		{
			$(this).parent().parent().remove();
		});
	}
	else
	{
		$('#image_container .image .delete').off('click');
	}
}


function savePhotos()
{
	var i = 0;
	var photos = [];
	$('#image_container .image').each(function()
	{
		photos.push(
		{
			'ordering': i++,
			'fname': $(this).find('.imagefile').attr('src'),
		});
	});
	
	$.ajax(
	{
		url: rootUrl + 'save_exhibition_photos',
		data: {
			'photos': photos,
			'exhibition_id': exhibition_id,
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				backendDialog('success','Save successful!', '');
			}
			else
			{
				backendDialog('error', 'Error while saving', '');
			}
		}
	});		
}