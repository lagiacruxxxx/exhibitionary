<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyRender
{
	protected $ci = null;
	protected $em = null;
	
	function __construct()
	{
	    $this->ci = & get_instance();
	    $this->ci->load->model('Exhibitionary_model', 'em');
	    $this->em = $this->ci->em;
    }	
    
    public function __renderBackend($view, $content_data)
    {
        $data = array();
        $data['username'] = MyAuth::getUser() != null ? MyAuth::getUser()->username : null;
        $data['additional_css'] = isset($content_data['additional_css']) ? $content_data['additional_css'] : array();
        $data['additional_js'] = isset($content_data['additional_js']) ? $content_data['additional_js'] : array();
        
        $this->ci->load->view('exhibitionary/head', $data);
        $this->ci->load->view('exhibitionary/menu', $data);
        $this->ci->load->view($view, $content_data);
        $this->ci->load->view('exhibitionary/footer', $data);        
    }
    
   
}


