<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyAuth extends MyRender
{
    protected $user = null;
    
	function __construct()
	{
        parent::__construct();
        $this->user = MyAuth::getUser();
    }	
    
    static function getUser()
    {
        $ci = & get_instance();
        $ci->load->model('Authentication_model', 'am');
        if($ci->session->userdata('user_id') != null)
        {
            return $ci->am->getAdmindataByID($ci->session->userdata('user_id'))->row();
        }
        else
        {
            return null;
        } 
    }
    
    public function user()
    {
        return $this->user;
    }
    
    public function logged_in()
    {
        return $this->user != null;
    }
    
    public function checkLogin()
    {
        if(!$this->logged_in())
        {
            $this->ci->session->set_userdata('exhibitionary_redirect', current_url());
            redirect('Authentication/login');
        }
    }
    
    public function showLogin($errormessage = '')
    {
        $data['errormessage'] = $errormessage;
        $this->__renderBackend('authentication/login', $data);
    }
    
    public function do_login()
    {
        $username = $this->ci->input->post('username');
        $pword = $this->ci->input->post('pword');
        $pw = $this->ci->am->getAdminPW($username);
        if ($pw->num_rows() > 0)
        {
            $pwcheck = MyAuth::__check_hash($pword, $pw->row()->pword);
            if ($pwcheck)
            {
                $userId = $this->ci->am->getAdmindataByUsername($username)->row()->id;
                $this->ci->session->set_userdata('user_id', $userId);
                if($this->ci->session->userdata('exhibitionary_redirect') !=  null)
                    redirect($this->ci->session->userdata('exhibitionary_redirect'));
                else
                    redirect(site_url());
            }
            else
            {
                $this->showLogin('Password incorrect');
            }
        }
        else
        {
            $this->showLogin('User not found');
        }
    }
    
    public function logout()
    {
        $this->ci->session->unset_userdata('user_id');
        $this->ci->session->sess_destroy();
        $this->user = null;
        redirect(site_url(''));
    }
    
    static function __check_hash($hash, $stored_hash)
    {
        require_once (APPPATH . 'libraries/PasswordHash.php');
        $hasher = new PasswordHash(8, false);
        return $hasher->CheckPassword($hash, $stored_hash);
    }
    
    static function __createHash($hash)
    {
        require_once(APPPATH.'libraries/PasswordHash.php');
        $hasher = new PasswordHash(8, false);
        return $hasher->HashPassword($hash);
    }
    
}


