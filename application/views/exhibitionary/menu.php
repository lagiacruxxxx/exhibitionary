
<div id="messagecontainer"></div>

<div id="menu">
	<?php if($username != null):?>
	<ul>
		<li><a href="<?= site_url('Authentication/logout')?>">Logout</a></li>
		<li><a href="<?= site_url('Authentication/adminsettings')?>"><img src="<?= site_url('items/exhibitionary/img/settings.png')?>" /></a></li>
		<li>Logged in as <b><?= $username?></b></li>
	</ul>
	<?php endif;?>
</div>

<div id="sidebar">
    <div class="sidebar_logo">
        <img src="<?= site_url('items/exhibitionary/img/logo.png')?>" />
    </div>
    
    <?php if($username != null):?>
    	<ul>
    		<li class="sidebar_headline">USER MANAGEMENT</li>
    		<li class="sidebar_menuitem">
                <a href="<?= site_url('administrator')?>">Administrators</a>
            </li>
    		<li class="sidebar_menuitem">
                <a href="<?= site_url('user')?>">Users</a>
            </li>

            <li><div class="separator"></div></li>
            
            <li class="sidebar_headline">CONTENT</li>
            <li class="sidebar_menuitem">
                <a href="<?= site_url('exhibition')?>">Exhibitions</a>
            </li>
            <li class="sidebar_menuitem">
                <a href="<?= site_url('venue')?>">Venue</a>
            </li>
            

            <li><div class="separator"></div></li>
            
            <li class="sidebar_headline">PICKS</li>
            <?php foreach($this->em->getCities()->result() as $city):?>
            <li class="sidebar_menuitem">
                <a href="<?= site_url('picks/' . $city->id)?>"><?= $city->name?></a>
            </li>
            <?php endforeach;?>
            
            <li><div class="separator"></div></li>
            
            <li class="sidebar_headline">SETTINGS</li>
            <li class="sidebar_menuitem">
                <a href="<?= site_url('city')?>">Cities</a>
            </li>
            <li class="sidebar_menuitem">
                <a href="<?= site_url('quarter')?>">Quarters</a>
            </li>
            
            <li><div class="separator"></div></li>
            
            
    	</ul>
	<?php endif;?>
</div>

