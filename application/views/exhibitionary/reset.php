
    <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/reset.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url("items/exhibitionary/css/pw_reset.css"); ?>">

    <div id="content">
    	<img class="logo" src="<?= site_url('items/exhibitionary/img/logo.png')?>" />
    	
    	<div class="regular" id="HL" style="font-size:36px;margin:35px 0px;">Exhibitionary</div>
    
    	<div class="regular" id="flow" style="font-size:18px;">You can reset your password here<br/><br/></div>
    	
    	<div id="forgot" style="display:block;">
    	<?php echo form_open("Authentication/reset_password_form");?>	
    	      <div style="margin-left:0px;text-align:left;">
    	      		
    	      		<input type="password" name="register_pw" placeholder="PASSWORD" value="" id="register_pw" class="login_input">	    
    	      		<input type="password" name="register_pw_conf" placeholder="PASSWORD CONFIRM" value="" id="register_pw_conf" class="login_input">	
    	      		<input type="hidden" name="register_hash" value="<?= $hash?>" id="register_hash" class="login_input">	   
    		  		<input type="submit" class="button regular" action="login" source="login" value="SUBMIT">
    	     </div>					
    	<?php echo form_close();?>
    	</div>
    	<?if(isset($message) && $message != "") echo $message;?>	
    </div>		