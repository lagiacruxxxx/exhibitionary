<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('NEWSLETTER_NO', 0);
define('NEWSLETTER_YES', 1);

define('USER_INACTIVE', 0);
define('USER_ACTIVE', 1);

define('EXHIBITION_HIDDEN', 0);
define('EXHIBITION_VISIBLE', 1);

define('VENUE_HIDDEN', 0);
define('VENUE_VISIBLE', 1);


define('NOVENUE', 0);

$config['dummy'] = array();
