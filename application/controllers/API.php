<?php defined('BASEPATH') OR exit('No direct script access allowed');

class API extends MY_Controller 
{
	
    function __construct()
    {
		parent::__construct();
		
		$this->load->model('Exhibitionary_model', 'em');
    }  
    

	public function index()
	{
        echo "NOT ALLOWED!";
	}
	
	function checkSession($session_id, $user_id = null){
		
		if($user_id == null){
			$session = $this->em->checkSession($session_id);
		}
		else{	
			$session = $this->em->checkSessionUser($session_id,$user_id);
		}
		
		if($session == "NO"){
			return "INVALID";
		}
		else{
			if(strtotime($session->last_used_date) >= strtotime("-60 minutes")){
			  $this->em->updateSession($session_id);
			  return "OK";
			}else{
			  return "EXPIRED";
			}
		}
	}
	
	public function register()
	{
	    /* Test data*/
	   /* $_POST['firstname'] = "S";
	    $_POST['lastname'] = "G";
	
	    $_POST['email'] = "g@c.com";
	    $_POST['password'] = "a";
	    $_POST['fbID'] = "0";
	    $_POST['newsletter'] = "1";
	    $_POST['role'] = "Dealer/Gallerist";*/
	
	
	    $firstname = $this->input->post('firstname'); //$_POST['firstname'];
	    $lastname = $this->input->post('lastname'); //$_POST['lastname'];
	    $username = $firstname." ".$lastname;
	    $email = $this->input->post('email'); //$_POST['email'];
	    $password = $this->input->post('password'); //$_POST['password'];
	    $fbId = $this->input->post('fbID'); //$_POST['fbID'];
	    $newsletter = $this->input->post('newsletter'); //$_POST['newsletter'];
	    $role = $this->input->post('role'); //$_POST['role'];
	
	
	    //validate form input
	    $this->load->library('ion_auth');
	    
	    $pw = MyAuth::__createHash($password);
	    
        $additional_data = array(
            'first_name' => $firstname,
            'last_name'  => $lastname,
            'role'		 => $role,
            'newsletter' => $newsletter == 1 ? 1 : 0,
        );
	    	
	    // Validate e-mail
	    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false)
	    {
	        $user_id = $this->ion_auth->register($username, $pw, $email, $fbId, $additional_data, array(2));
	        	
	        if ($user_id !== FALSE)
	        {
	            $response_data['response']['status'][] = array(
	                'id' => $user_id,
	                'status' => "OK",
	            );
	        }
	        else
	        {
	            $response_data['response']['status'][] = array(
	                'id' => "00",
	                'status' => "FAIL"
	            );
	        }
	    }
	    else
	    {
	        $response_data['response']['status'][] = array(
	            'id' => "00",
	            'status' => "EMAIL"
	        );
	    }
	
	    $json_data = json_encode($response_data, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	function reset_password()
	{
	    //$_POST['email'] = "benjamin.schneller@gmx.at";
	
	    $email = $_POST['email'];
	
	    if($email)
	    {
	        $rand_string = rand_string(40);
	        	
	        $user = $this->em->getUser($email)->row();
	        	
	        $data = array('forgotten_password_code' => $rand_string);
	        	
	        $this->em->updateUserData($data, $user->id);
	        	
	        $recipient = $user->first_name." ".$user->last_name;
	        	
	        $msg = $this->send_mail_reset($email, $recipient, $rand_string);
	        	
	        	
	        	
	        $picture_item = array('id' => $user->id,
	            'status' => "OK");
	        	
	        $albums_data['response']['status'][] = $picture_item;
	        	
	    }
	    else
	    {
	        $picture_item = array('id' => "00",
	            'status' => "EMAIL");
	
	        $albums_data['response']['status'][] = $picture_item;
	    }
	
	
	    $json_data = json_encode($albums_data, JSON_PRETTY_PRINT);
	    echo $json_data;
	
	
	
	}
	
	
	public function send_mail_reset($email, $recipient, $hash) 
	{
	    $this->load->library('My_phpmailer');
	
	    $mail = new PHPMailer();
	    $mail->IsSendmail(); // we are going to use SMTP
	    $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
	    $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server
	    $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
	    $mail->Port       = 465;                   // SMTP port to connect to
	    $mail->Username   = "1590mail27";  // user email address
	    $mail->Password   = "Kksnoopstr";            // password
	    $mail->SMTPAuth   = true; // enabled SMTP authentication
	    $mail->SetFrom('noreply@exhibitionary.com', 'Exhibitionary');  //Who is sending the email
	    $mail->AddReplyTo("noreply@exhibitionary.com","Exhibitionary");  //email address that receives the response
	
	     
	    $mail->Subject    = "Exhibitionary password reset";
	    $mail->Body      = "<p>Hello ".$recipient."!</p><br><p>You have requested a password reset for Exhibitionary</p><br/><p>Click the link below to reset your password:</p><br/><a href='".site_url('reset_password/'.$hash)."'>RESET PASSWORD</a>";
	    $mail->AltBody    = "Hello ".$recipient."! You have requested a password reset for Exhibitionary. Click the link to reset your password:<a href='".site_url('reset_password/'.$hash)."'>RESET PASSWORD</a>";
	
	     
	    $mail->AddAddress($email, $recipient);
	    //$mail->Send();
	    if(!$mail->Send()) {
	        $data["message"] = "Error: " . $mail->ErrorInfo;
	    } else {
	        $data["message"] = "Message sent correctly!";
	    }
	    return $data['message'];
	}	
	
	function login()
	{
	    if(isset($_POST['password']) && isset($_POST['username']) && !empty($_POST['password']) && !empty($_POST['username']))
	    {
	        $inputPW = $_POST['password'];
	        $inputEmail = $_POST['username'];
	        	
	        $stored = $this->em->checkMobilePW($inputEmail);
	        	
	        if($stored)
	        {
	            $storedPW = $stored->row()->password;
	            $pwCheck = MyAuth::__check_hash($inputPW, $storedPW);
	        }
	    }
	    else{
	
	        $inputPW = "";
	        $inputEmail = "";
	        $pwCheck = false;
	    }
	
	    $response = array();
	    $sess_id = "00";
	
	    if ($pwCheck && isset($inputPW) && isset($inputEmail))
	    {
	        //if the login is successful
	        $data = array();
	        $ip = $this->input->ip_address();
	        $random_string = rand_string(16);
	        $result = $this->em->checkLoginUserFB($inputEmail)->row();
	        $user_id = $result->id;
	        	
	        $data = array(	"session_id" => $random_string,
	            "ip_address" => $ip,
	            "user_id" => $user_id,
	            "created_date" => date('Y-m-d H:i:s'),
	            "last_used_date" => date('Y-m-d H:i:s'),
	        );
	        $sess_id = $this->em->saveSession($data);
	        	
	        	
	        $response['response'] = array("id" => $sess_id,
	            'status' => "OK",
	            'session_id' => $random_string,
	            'user_id' => $user_id,
	            'error' => "-");
	    }
	    else
	    {
	        //if the login was un-successful
	        $response['response'] = array("id" => $sess_id,
	            'status' => "FAIL",
	            'session_id' => "-",
	            'user_id' => "-",
	            'error' => "Invalid email or password!");
	        	
	        	
	    }
	    	
	    $json_data = json_encode($response, JSON_PRETTY_PRINT);
	    echo $json_data;
	
	}	
	
	
	public function loginUserFB(){
	
	    /* Test data */
	    /*	$_POST['fb_id'] = "100001607760646";
	     $_POST['email'] = "loi2di@hotmail.com";
	     */
	
	    $fbId = $_POST['fb_id'];
	    $email = $_POST['email'];
	    $token = $_POST['token'];
	
	    $response = array();
	    $sess_id = "00";
	
	    if($this->em->checkFbId($fbId))
	    {
	        // FOUND FB ID
	        	
	        $token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
	        $fb_id = json_decode($token_check)->id;
	        	
	        $token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
	        $app_id = json_decode($token_check)->id;
	        	
	        $emailCheck = $this->em->checkFbToEmail($fbId, $email);
	        	
	        if($fb_id == $fbId && $app_id == "1708622379405758" && $emailCheck != FALSE)
	        {
	            //LOGIN
	            $random_string = rand_string(16);
	            $ip = $this->input->ip_address();
	            $result = $this->em->checkFbId($fbId)->row();
	
	            $user_id = $result->id;
	
	            $data = array(	"session_id" => $random_string,
	                "ip_address" => $ip,
	                "user_id" => $user_id,
	                "created_date" => date('Y-m-d H:i:s'),
	                "last_used_date" => date('Y-m-d H:i:s'),
	            );
	            	
	            $sess_id = $this->em->saveSession($data);
	
	
	
	            $response['response'] = array("id" => $sess_id,
	                'status' => "OK",
	                'session_id' => $random_string,
	                'user_id' => $user_id,
	                'error' => "-");
	            	
	
	
	            $json_data = json_encode($response, JSON_PRETTY_PRINT);
	            echo $json_data;
	
	        }
	        else{
	            //INVALID FBID
	            $response['response'] = array("id" => $sess_id,
	                'status' => "FAIL",
	                'session_id' => "-",
	                'user_id' => "-",
	                'error' => "ID or Email invalid!");
	            $json_data = json_encode($response, JSON_PRETTY_PRINT);
	            echo $json_data;
	        }
	        	
	
	    }
	    else
	    {
	        // NO FB ID
	        	
	        if($this->em->checkLoginUserFB($email) != false)
	        {
	            // FOUND EMAIL
	
	
	
	            $token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
	            $fb_id = json_decode($token_check)->id;
	            $token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
	            $app_id = json_decode($token_check)->id;
	            	
	            if($fb_id == $fbId && $app_id == "1708622379405758")
	            {
	                $updated = array('fb_id' => $fbId);
	
	                $this->em->updateProfilePic($email, $updated);
	                //LOGIN
	                $random_string = rand_string(16);
	                $ip = $this->input->ip_address();
	                $result = $this->em->checkFbId($fbId)->row();
	                	
	                $user_id = $result->id;
	                	
	                $data = array(	"session_id" => $random_string,
	                    "ip_address" => $ip,
	                    "user_id" => $user_id,
	                    "created_date" => date('Y-m-d H:i:s'),
	                    "last_used_date" => date('Y-m-d H:i:s'),
	                );
	
	                $sess_id = $this->em->saveSession($data);
	                	
	                	
	                	
	                $response['response'] = array("id" => $sess_id,
	                    'status' => "OK",
	                    'session_id' => $random_string,
	                    'user_id' => $user_id,
	                    'error' => "-");
	
	                	
	                	
	                $json_data = json_encode($response, JSON_PRETTY_PRINT);
	                echo $json_data;
	                	
	            }
	            else{
	                //INVALID FBID
	                $response['response'] = array("id" => $sess_id,
	                    'status' => "FAIL",
	                    'session_id' => "-",
	                    'user_id' => "-",
	                    'error' => "ID invalid!");
	                $json_data = json_encode($response, JSON_PRETTY_PRINT);
	                echo $json_data;
	            }
	        }
	        else
	        {
	            // NO EMAIL - ADD THE USER
	
	            $token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
	            $fb_id = json_decode($token_check)->id;
	            $user_data = json_decode($token_check);
	
	            $token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
	            $app_id = json_decode($token_check)->id;
	
	
	            //	if($fb_id == $fbId && $app_id == "1708622379405758")
	            //	{
	            $username = $user_data->name;
	            	
	            	
	            $username_arr = explode(' ', $username);
	            	
	            	
	            $pw =  rand_string(16);
	            $additional_data = array(
	                'first_name' => $username_arr[0],
	                'last_name'  => $username_arr[1],
	                'newsletter' => 1,
	            );
	            	
	            	
	            	
	            $this->ion_auth->register($username, $pw, $email, $fbId, $additional_data);
	            	
	            	
	            //LOGIN
	            $random_string = rand_string(16);
	            $ip = $this->input->ip_address();
	            $result = $this->em->checkFbId($fbId)->row();
	            	
	            $user_id = $result->id;
	            	
	            $data = array(	"session_id" => $random_string,
	                "ip_address" => $ip,
	                "user_id" => $user_id,
	                "created_date" => date('Y-m-d H:i:s'),
	                "last_used_date" => date('Y-m-d H:i:s'),
	            );
	
	            $sess_id = $this->em->saveSession($data);
	            	
	            	
	            	
	            $response['response'] = array("id" => $sess_id,
	                'status' => "OK",
	                'session_id' => $random_string,
	                'user_id' => $user_id,
	                'error' => "-");
	            /*	}
	             else
	             {
	             //INVALID FBID
	             $response['response'] = array("id" => $sess_id,
	             'status' => "FAIL",
	             'session_id' => "-",
	             'user_id' => "-",
	             'error' => "ID invalid!");
	             	
	             }
	            */
	
	            $json_data = json_encode($response, JSON_PRETTY_PRINT);
	            echo $json_data;
	            	
	
	        }
	    }
	
	
	
	}
	
	public function user_favourites(){
	
	
	    if(isset($_POST['user_id']))
	    {
	        $id = $_POST['user_id'];
	
	        $car_container = array();
	        	
	        if(!empty($_POST['session_id']))
	        {
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	            $session_data = $this->em->sessionData($session_id);
	        }
	        else
	            $session_response = "INVALID";
	        	
	        	
	        $car_container['response']['session'] = array("id" => $id,
	            "status" => $session_response);
	        	
	        if($session_response == "OK")
	        {
	            $favourites = $this->em->getFavourites($id)->result();
	
	            foreach($favourites as $item)
	            {
	                $car_item = array(
	                    'eyeout_id' => $item->exhibition_id);
	
	                $car_container['response']['favourites'][] = $car_item;
	            }
	        }
	        else
	        {
	            $car_item = array(
	                'eyeout_id' => "00");
	
	            $car_container['response']['favourites'][] = $car_item;
	        }
	        	
	        	
	    }
	    else
	    {
	        if(!empty($_POST['session_id'])){
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	        }
	        	
	        $car_container['response']['session'] = array("id" => "00",
	            "status" => $session_response);
	        	
	        $car_item = array('eyeout_id' => "00");
	
	        $car_container['response']['favourites'][] = $car_item;
	    }
	
	    $json_data = json_encode($car_container, JSON_PRETTY_PRINT);
	    echo $json_data;
	}	
	
	
	public function add_favourite()
	{
	    $user_id = $_POST['user_id'];
	    $eyeout_id = $_POST['eyeout_id'];
	
	    $session_response = "INVALID";
	    $container = array();
	
	    if(!empty($_POST['session_id'])){
	        $session_id = $_POST['session_id'];
	        $session_response = $this->checkSession($session_id);
	    }
	
	    $container['response']['session'] = array("id" => $user_id,
	        "status" => $session_response);
	
	    if($session_response == "OK")
	    {
	        	
	        $data = array('exhibition_id' => $eyeout_id,
	            'user_id' => $user_id);
	        	
	        $this->em->addFavourite($data);
	        	
	        $container_item = array("id" => $eyeout_id,
	            "status" => "OK");
	        $container['response']['response'][] = $container_item;
	    }
	    else
	    {
	        $container['response']['response'][] = array("id" => "00",
	            "status" => "INVALID SESSION");
	    }
	    $json_data = json_encode($container, JSON_PRETTY_PRINT);
	    echo $json_data;
	
	}
	
	public function delete_favourite()
	{
	
	    $user_id = $_POST['user_id'];
	    $eyeout_id = $_POST['eyeout_id'];
	
	    $session_response = "INVALID";
	    $container = array();
	
	    if(!empty($_POST['session_id'])){
	        $session_id = $_POST['session_id'];
	        $session_response = $this->checkSession($session_id);
	    }
	
	    $container['response']['session'] = array("id" => $user_id,
	        "status" => $session_response);
	
	    if($session_response == "OK")
	    {
	        $data = array('eyeout_id' => $eyeout_id,
	            'user_id' => $user_id);
	        	
	        $this->em->deleteFavourite($user_id,$eyeout_id);
	        	
	        $container_item = array("id" => $eyeout_id,
	            "status" => "OK");
	        $container['response']['response'][] = $container_item;
	
	    }
	    else
	    {
	        $container['response']['response'][] = array("id" => "00",
	            "status" => "INVALID SESSION");
	    }
	    $json_data = json_encode($container, JSON_PRETTY_PRINT);
	    echo $json_data;
	    	
	
	}	
	
	
	public function check_favourite()
	{
	    if(isset($_POST['user_id']))
	    {
	        $id = $_POST['user_id'];
	        $eye_id = $_POST['eyeout_id'];
	
	        $car_container = array();
	
	        $session_response = "INVALID";
	        	
	        if(!empty($_POST['session_id'])){
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	            $session_data = $this->em->sessionData($session_id);
	        }
	        	
	        $car_container['response']['session'] = array("id" => $id,
	            "status" => $session_response);
	        	
	        if($session_response == "OK")
	        {
	            $favourite = $this->em->checkFavourite($id, $eye_id);
	            $car_item = array(
	                'is_favourite' => $favourite);
	            	
	            $car_container['response']['favourite'][] = $car_item;
	        }
	        else
	        {
	            $car_item = array(
	                'is_favourite' => false);
	            	
	            $car_container['response']['favourite'][] = $car_item;
	        }
	    }
	    else
	    {
	        if(!empty($_POST['session_id'])){
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	        }
	        	
	        $car_container['response']['session'] = array("id" => "00",
	            "status" => $session_response);
	        	
	        $car_item = array('is_favourite' => false);
	
	        $car_container['response']['favourite'][] = $car_item;
	    }
	
	    $json_data = json_encode($car_container, JSON_PRETTY_PRINT);
	    echo $json_data;
	}	
	
	
	public function feed()
	{
	    if($this->input->get('id') != null)
	    {
	        $id = $this->input->get('id');
	
            $feeditems = array();
            foreach($this->em->getCityFeed($id)->result() as $item)
            {
                $feeditems[] = array(
                    'exhibition_id' => $item->exhibition_id
                );
            }
            
            $response['response'] = array(
                'session' => array(
                    "id" => $id,
                    "status" => "OK"
                ),
                'feeditems' => $feeditems,
            );
	    }
	    else
	    {
	        if(!empty($_POST['session_id']))
	        {
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	        }
	        	
	        $response['response'] = array(
	            'session' => array(
                    "id" => "00",
                    "status" => "INVALID",
	            ),
	            'feeditems' => array(
                    'exhibition_id' => "00",
	            ),
	        );
	    }
	
	    $json_data = json_encode($response, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	
	
	public function add_stat()
	{
	    $user_id = $_POST['user_id'];
	    $category = $_POST['category'];
	    $page_id = (isset($_POST['page_id']))? $_POST['page_id'] : 0;
	    $session_id = $_POST['session_id'];
	    $session_response = "OK";
	    $container = array();
	
	    $container['response']['session'] = array("id" => $user_id,
	        "status" => $session_response);
	
	    if($session_response == "OK")
	    {
	        $data = array('category' => $category,
	            'user_id' => $user_id,
	            'page_id' => $page_id,
	            'session_id' => $session_id);

	        $retnum = $this->em->addStat($data);
	        $container_item = array(
	            "id" => $user_id,
	            "status" => "OK");
	        $container['response']['response'][] = $container_item;
	    }
	    else
	    {
	        $container['response']['response'][] = array("id" => "00",
	            "status" => "INVALID SESSION");
	    }
	    $json_data = json_encode($container, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	
	
	public function contributor_feed()
	{
	    if(isset($_POST['contributor_id']))
	    {
	        $id = $_POST['contributor_id'];
	
	        $car_container = array();
	
	        $session_response = "OK";
	        /*
	         if(!empty($_POST['session_id'])){
	         $session_id = $_POST['session_id'];
	         $session_response = $this->checkSession($session_id);
	         $session_data = $this->auth_model->sessionData($session_id);
	         }
	         	
	         */
	        	
	        $car_container['response']['session'] = array("id" => $id,
	            "status" => $session_response);
	        	
	        if($session_response == "OK")
	        {
	            $favourites = $this->em->getContributorFeed($id)->result();
	
	            foreach($favourites as $item)
	            {
	                $car_item = array(
	                    'eyeout_id' => $item->exhibition_id);
	
	                $car_container['response']['favourites'][] = $car_item;
	            }
	        }
	        else
	        {
	            $car_item = array(
	                'eyeout_id' => "00");
	
	            $car_container['response']['favourites'][] = $car_item;
	        }
	        	
	    }
	    else
	    {
	        if(!empty($_POST['session_id'])){
	            $session_id = $_POST['session_id'];
	            $session_response = $this->checkSession($session_id);
	        }
	        	
	        $car_container['response']['session'] = array("id" => "00",
	            "status" => $session_response);
	        	
	        $car_item = array('eyeout_id' => "00");
	
	        $car_container['response']['favourites'][] = $car_item;
	    }
	
	    $json_data = json_encode($car_container, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	
	
	
	
	function getExhibitions()
	{
	    $city_id = $this->input->get('city_id');
	    $limit = $this->input->get('limit') != null ? $this->input->get('limit') : 99999;
	    $offset = $this->input->get('offset') != null ? $this->input->get('offset') : 0;
	    
	    $result = array();
	    foreach($this->em->getAPIgetExhibitions($city_id, $limit, $offset)->result() as $ex)
	    {
	        $result[] = $this->echoExhibitionData($ex->id);
	    }
	    
	    $json_data = json_encode($result, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	function getFutureExhibitions()
	{
	    $city_id = $this->input->get('city_id');
	    $limit = $this->input->get('limit') != null ? $this->input->get('limit') : 99999;
	    $offset = $this->input->get('offset') != null ? $this->input->get('offset') : 0;
	     
	    $result = array();
	    foreach($this->em->getAPIgetFutureExhibitions($city_id, $limit, $offset)->result() as $ex)
	    {
	        $result[] = $this->echoExhibitionData($ex->id);
	    }
	     
	    $json_data = json_encode($result, JSON_PRETTY_PRINT);
	    echo $json_data;	    
	}
	
	function getExhibitionsByIdArray()
	{
	    $array = explode(',', $this->input->get('list'));
	    $limit = $this->input->get('limit') != null ? $this->input->get('limit') : 99999;
	    $offset = $this->input->get('offset') != null ? $this->input->get('offset') : 0;
	    
	    $result = array();
	    foreach($this->em->getAPIgetExhibitionsArray($array, $limit, $offset)->result() as $ex)
	    {
	        $result[] = $this->echoExhibitionData($ex->id);
	    }
	    
	    $json_data = json_encode($result, JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	function getExhibitionsById()
	{
	    $id = $this->input->get('id');
	    
	    $json_data = json_encode(array($this->echoExhibitionData($id)), JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	function echoExhibitionData($id)
	{
	    $exhibition = $this->em->getAPIExhibitionById($id)->row();
	     
	    if($exhibition->venue_id != NULL)
	        $venue = $this->em->getAPIVenueById($exhibition->venue_id)->row();
	    else
	        $venue = null;
	     
	    $result = array();
	    $result['created_at'] = null;
	    $result['updated_at'] = null;
	    $result['id'] = intval($exhibition->id);
	    $result['eyeout_id'] = null;
	    $result['hidden'] = !$exhibition->visible;
	    $result['opening_end'] = $exhibition->opening_start != null && $exhibition->opening_end_time != '' ? $exhibition->opening_start . 'T' . $exhibition->opening_end_time . ':00+00:00' : null;
	    $result['opening_start'] = $exhibition->opening_start != null && $exhibition->opening_start_time != ''  ? $exhibition->opening_start . 'T' . $exhibition->opening_start_time . ':00+00:00' : null;
	    $result['end'] = $exhibition->enddate;
	    $result['start'] = $exhibition->startdate;
	    $result['special_id'] = null;
	    $result['text'] = $exhibition->description;
	    $result['title'] = $exhibition->title;
	    $result['updated_at'] = null;
	    $result['city_id'] = intval($exhibition->city_id);
	    $result['city_name'] = $exhibition->city_name;
	    $result['venue_addition'] = $venue == null ? null : intval($exhibition->venue_id);
	    $result['venue_city'] = $venue == null ? null : intval($venue->city_id);
	    $result['venue_city_name'] = $venue == null ? null : $venue->city_name;
	    $result['venue_email'] = $venue == null ? null : $venue->email;
	    $result['venue_hours'] = $venue == null ? null : $venue->hours;
	    $result['venue_id'] = $venue == null ? null : intval($venue->id);
	    $result['venue_lat'] = $venue == null ? null : $venue->gl_lat;
	    $result['venue_long'] = $venue == null ? null : $venue->gl_long;
	    $result['venue_name'] = $venue == null ? null : $venue->name;
	    $result['venue_phone'] = $venue == null ? null : $venue->phone;
	    $result['venue_postcode'] = $venue == null ? null : $venue->postcode;
	    $result['venue_quarter'] = $venue == null ? null : $venue->quarter_name;
	    $result['venue_quarter_id'] = intval($venue == null ? null : $venue->quarter);
	    $result['venue_sort'] = $venue == null ? null : $venue->sort;
	    $result['venue_street'] = $venue == null ? null : $venue->street;
	    $result['venue_web'] = $venue == null ? null : $venue->web;
	    $result['images'] = array();
	    foreach($this->em->getAPIExhibtionPhotos($exhibition->id)->result() as $photo)
	    {
	        $result['images'][] = array(
	            'created_at' => null,
	            'id' => intval($photo->id),
	            'imageable_id' => null,
	            'imageable_type' => null,
	            'order' => intval($photo->ordering),
	            'retina_image' => false,
	            'title' => '',
	            's3_id' => null,
	            'updated_at' => null,
	            'url' => 'http://54.229.72.229' . $photo->fname,
	        );
	    }
	    
	    return $result;
	}
	
	
	function getVenueById()
	{
	    $id = $this->input->get('id');
	    $venue = $this->em->getAPIVenueById($id)->row();
	    
	    $result['id'] = intval($venue->id);
	    $result['name'] = $venue->name;
	    $result['street'] = $venue->street;
	    $result['quarter'] = $venue->quarter_name;
	    $result['quarter_id'] = intval($venue->quarter);
	    $result['sort'] = $venue->sort;
	    $result['postcode'] = $venue->postcode;
	    $result['city_id'] = intval($venue->city_id);
	    $result['city'] = $venue->city_name;
	    $result['phone'] = $venue->phone;
	    $result['web'] = $venue->web;
	    $result['email'] = $venue->email;
	    $result['hours'] = $venue->hours;
	    $result['lat'] = $venue->gl_lat;
	    $result['long'] = $venue->gl_long;
	    $result['hidden'] = !$venue->visible;
	    $result['stop'] = $venue->stop;
	    $result['subway'] = $venue->subway;
	    
	    $json_data = json_encode(array($result), JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	
	function getCities()
	{
	    $result = array();
	    foreach($this->em->getVisibleCities()->result() as $city)
	    {
	        $result[] = array(
                'id' => intval($city->id),
	            'name' => $city->name,
	            'lat' => $city->gl_lat,
	            'long' => $city->gl_long,
	        );
	    }
	    
	    $json_data = json_encode($result, JSON_PRETTY_PRINT);
	    echo $json_data;	    
	}
	
	function getNearestCity()
	{
	    $lat = doubleval($this->input->get('lat'));
	    $long = doubleval($this->input->get('long'));
	    
	    $dist = 9999999999999;
	    $nearestCity = null;
	    $nearestCityName = '';
	    foreach($this->em->getVisibleCities()->result() as $city)
	    {
	        $new_dist = $this->vincentyGreatCircleDistance($lat, $long, $city->gl_lat, $city->gl_long);
	        if($new_dist < $dist)
	        {
	            $dist = $new_dist;
	            $nearestCity = $city->id;
	            $nearestCityName = $city->name;
	        }
	    }
	    
	    $json_data = json_encode(array(
	        'id' => $nearestCity,
	        'name' => $nearestCityName,
	        'distance' => $dist,
	    ), JSON_PRETTY_PRINT);
	    echo $json_data;
	}
	
	private static function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
	    // convert from degrees to radians
	    $latFrom = deg2rad($latitudeFrom);
	    $lonFrom = deg2rad($longitudeFrom);
	    $latTo = deg2rad($latitudeTo);
	    $lonTo = deg2rad($longitudeTo);
	     
	    $lonDelta = $lonTo - $lonFrom;
	    $a = pow(cos($latTo) * sin($lonDelta), 2) +
	    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
	    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
	     
	    $angle = atan2(sqrt($a), $b);
	    return $angle * $earthRadius;
	}
	
	/*function fixgeoloc()
	{
	    foreach($this->em->getVenues()->result() as $venue)
	    {
	        if($venue->gl_long != '' && $venue->gl_lat != '')
	        {
	        //echo $venue->gl_long . ' - ';
	        $long = explode('.', $venue->gl_long);
	        //$long = $long[0] . '.' . $long[1] . $long[2] . '<br>';
            $lat = explode('.', $venue->gl_lat);
	        
            $loc = array(
                'gl_long' => $long[0] . '.' . $long[1] . $long[2],
                'gl_lat' => $lat[0] . '.' . $lat[1] . $lat[2],
            );
            $this->em->updateVenue($venue->id, $loc);
	        }
	    }
	    echo "done";
	}*/
	
	
	/*function importVenue($c)
	{
	    
	    $filename = $c == 'ber' ? 'ber_venue.csv' : 'nyc_venue.csv';
	    $city = $c == 'ber' ? 2 : 4;
	    $quarter_start_id = $c == 'ber' ? 1 : 15;
	    $xref = array('C10000','C10002','C10003','C10004','C10856','C10966','G10008','G10637','G10010','G10381','G10012','G10861','G10014','G10016','G10017','G10830','G10941','G10019','G10020','G10021','G10023','G10024','G10026','G10957','G10027','G10827','G10943','G10218','G10890','G10076','G10029','G10032','G10619','G10197','G10035','G10369','G11076','G10633','G10224','G10037','G10968','G10370','G10881','G10042','G10942','G10896','G10750','G10045','G10047','G10048','G10803','G10049','G10834','G10053','G10132','G10056','G10790','G10057','G10058','G10958','G10959','G10061','G10062','G10064','G10910','G10067','G10913','G10068','G10071','G10811','G10365','G10072','G10073','G10805','G10634','G10075','G10077','G10078','G10443','G10809','G10137','G10435','G10081','G10136','G10034','G10082','G10086','G10087','G11047','G10088','G10367','G10089','G10091','M10431','M10092','M11095','M11096','M10114','M10096','M10100','M10103','M10106','M10107','M10108','M10937','M10111','M10115','M10120','M10126','O10862','Y11089','Y10918','Y11078','Y11077','Y10375','Y10922','Y11091','Y11080','Y10924','Y10947','Y10951','Y10821','Y10863','Y11094','Y11088','Y10895','Y11081','Y10928','Y11090','Y11092','Y11082','Y10201','Y10878','Y11085','Y10929','Y10944','Y11086','Y11087','Y11083','Y11084','Y11093','Y10932','Y11079','Y11097','Y10933','G11007','G11008','G11009','G11099','G11100','G11010','G11011','G10997','G11112','G10972','G10991','G10973','G11129','G10974','G10978','G10976','G11019','G11018','G11042','G11043','G11041','G11021','G10979','G11023','G11025','G11026','G10980','G11052','G10985','G10981','G10988','G11110','G11111','G11039','G11017','G10982','G11075','G10983','G11113','G11053','G11045','G11044','G11068','G11046','G10970','G11051','G11050','G11128','G11067','G11132','G11055','G11024','G10996','G11059','G11119','G10971','G10987','G11014','G11061','G11060','G11062','G11027','G11054','G11064','G11063','G10990','G11065','G11066','G11073','G11074','G11072','G10992','G10993','G11070','G11069','G11071','G11057','G11049','G10994','G10995','G11101','G11028','G11104','G11109','G11108','G11107','G10984','G11040','G11114','G11105','G10998','G11115','G11116','G11000','G11012','G11013','G11001','G11003','G11002','G11118','G11120','G11121','G11123','G11124','G11117','G11125','G11122','G11005','G11006','G10999','G11136','G11137','G11058','G11098','G11135','G11029','G11037','M11020','M11038','M11130','M11048','M11131','M11102','M11106','M11103','M11127','M11139','Y11015','Y11016','Y10986','Y11133','Y10989','Y11134','Y11056','Y11004','Y11138');
	    
	    $row = 1;
	    
	    echo "filename: $filename<br>";
	    echo "quarter start id: $quarter_start_id<br>";
	    echo "city: $city<br><br><br><br>";
	    
	    $this->em->deleteVenues($city);
	    $this->em->deleteQuarters($city);
	    
	    
	    echo "reading file<br>";
	    $venues = array();
	    $quarter = array();
    	if (($handle = fopen($filename, "r")) !== FALSE) 
    	{
            while (($data = fgetcsv($handle, 0, "|")) !== FALSE) 
            {
                if($data[0] != '' && isset($data[0]))
                    $venues[] = $data;
            }
            fclose($handle);
        }
        echo "reading file - DONE<br><hr>";
        

        echo "build quarter array<br>";
        $i = $quarter_start_id;
        for($key = 2 ; $key < count($venues) ; $key++)
        {
            //var_dump($key);
            $v = $venues[$key];
            if($v[5] == '')
                $v[5] = 'NO DATA';
            
            if(($quarterId = array_search($v[5], $quarter)) != false)
            {
                $venues[$key][5] = $quarterId;
            } 
            else
            {
                $quarter[$i] = $v[5];
                $venues[$key][5] = $i;
                $i++;
            }
        }
        echo "build quarter array - DONE<br><hr>";
        $i = $quarter_start_id;
        echo "insert quarters<br>";
        $batch = array();
        foreach($quarter as $key => $q)
        {
            $batch[] = array(
                'id' => $key,
                'name' => $q,
                'city_id' => $city,
            );
        }
        $this->em->insertQuarters($batch);
        echo "insert quarters - DONE<br><hr>";
        

        echo "insert venues<br>";
        $batch = array();
        for($key = 2 ; $key < count($venues) ; $key++)
        {
            $v = $venues[$key];
            
            $dummy = array(
                'eyeout_id' => $v[0],
                'name' => $v[1],
                'sort' => $v[2],
                'street' => $v[3],
                'quarter' => $v[5],
                'postcode' => $v[6],
                'city_id' => $city,
                'phone' => $v[8],
                'web' => $v[9],
                'email' => $v[10],
                'hours' => $v[11],
                'subway' => $v[12],
                'stop' => $v[13],
                'gl_lat' => $v[17],
                'gl_long' => $v[18],
                'visible' => 1,
            );
            
            $batch[] = $dummy;
        }
        
        $this->em->insertVenues($batch);
        
        echo "insert venues - DONE<br><hr>";
        
        echo "cross referencing real exhibitions<br><hr>";
        $this->em->xrefVenues($xref);
        echo "cross referencing real exhibitions - DONE<br><hr>";
	}*/
	
	/*function importExhibitions($c)
	{
	    $filename = $c == 'ber' ? 'ber_event.csv' : 'nyc_event.csv';
	    $city = $c == 'ber' ? 2 : 4;
	    $xref = array('E17166','E17176','E17182','E17183','E17177','E17178','E17179','E17185','E17187','E17186','E17188','E17165','E17189','E17190','E17191','E17192','E17194','E17195','E17196','E17197','E17198','E17199','E17200','E17201','E17202','E17204','E17203','E17170','E17205','E17238','E17206','E17207','E17208','E17210','E17209','E17211','E17172','E17171','E17214','E17215','E17216','E17217','E17173','E17218','E17219','E17162','E17164','E17220','E17221','E17222','E17224','E17223','E17225','E17226','E17227','E17169','E17168','E17181','E17163','E17228','E17229','E17230','E17231','E17232','E17233','E17234','E17235','E17236','E17237','E17239','E17240','E17241','E17242','E17245','E17244','E17243','E17246','E17247','E17248','E17249','E17250','E17251','E17252','E17253','E17254','E17255','E17256','E17258','E17257','E17259','E17260','E17167','E17293','E17295','E17294','E17296','E17299','E17297','E17298','E17300','E17301','E17302','E17303','E17305','E17304','E17306','E17307','E17309','E17308','E17310','E17311','E17312','E17313','E17317','E17315','E17314','E17316','E12919','E16454','E17012','E17000','E17175','E17149','E17158','E17031','E17030','E17143','E17144','E17145','E17039','E17040','E17132','E17133','E17047','E17146','E17081','E17150','E17154','E17153','E17050','E16922','E17138','E17088','E17155','E17142','E17139','E17087','E17135','E17028','E17134','E17152','E17060','E17140','E17151','E17006','E17136','E17083','E17147','E17148','E17089','E17137','E16966','E17156','E17127','E17129','E17128','E16987','E17160','E17161','E17094','E17093','E16783','E17078','E17070','E16985','E17141','E17076','E17071','E17073','E17079','E17125','E17122','E17124','E17117','E17119','E17121','E17120','E17108','E17110','E17123','E17101');
	    
	    echo "filename: $filename<br>";
	    echo "city: $city<br><br><br><br>";
	     
	    echo "reading file<br>";
	    $exhibitions = array();
    	if (($handle = fopen($filename, "r")) !== FALSE) 
    	{
            while (($data = fgetcsv($handle, 0, "|")) !== FALSE) 
            {
                if($data[0] != '' && isset($data[0]) && array_search($data[0], $xref) !== false)
                {
                    $exhibitions[] = $data;
                }
            }
            fclose($handle);
        }
        echo "reading file - DONE<br><hr>";
        
        $this->em->deleteExhibitions($city);
        
        echo "insert exhibitions<br>";
        $batch = array();
        for($key = 0 ; $key < count($exhibitions) ; $key++)
        {
            
            $v = $this->em->getVenueByEyeoutId($exhibitions[$key][1]);            
            $batch[] = array(
                'eyeout_id' => $exhibitions[$key][0],
                'title' => $exhibitions[$key][4],
                'description' => $exhibitions[$key][9], 
                'startdate' => date('Y-m-d', strtotime($exhibitions[$key][7])),
                'enddate' => date('Y-m-d', strtotime($exhibitions[$key][8])),
                'opening_start' => $exhibitions[$key][5]!= '' ? date('Y-m-d', strtotime($exhibitions[$key][5])) : null,
                'opening_start_time' => $exhibitions[$key][5] != '' ? date('H:i', strtotime($exhibitions[$key][5])) : null,
                'opening_end_time' => $exhibitions[$key][6] != '' ? date('H:i', strtotime($exhibitions[$key][6])) : 0,
                'venue_id' => $v->num_rows() != 1 ? null : $v->row()->id,
                'visible' => 1,
                'city_id' => $city,
            );
        }

        echo $this->em->insertExhibitions($batch) . ' rows inserted!<br>';

        echo "insert venues - DONE<br><hr>";
        
        
        echo "cross referencing real exhibitions<br><hr>";
        $this->em->xrefExhibitions($xref);
        echo "cross referencing real exhibitions - DONE<br><hr>";
        
        
        echo "process images<br><hr>";
        $batch = array();
        for($key = 0 ; $key < count($exhibitions) ; $key++)
        {
            $ordering = 0;
            $exId = $this->em->getExhibitionByEyeoutId($exhibitions[$key][0])->row()->id;
            foreach(explode(",", $exhibitions[$key][10]) as $fname)
            {
                $batch[] = array(
                    'exhibition_id' => $exId,
                    'fname' => '/exhibitionary/items/uploads/' . $fname . '.jpg',
                    'ordering' => $ordering++, 
                );
            }
        }
        echo $this->em->insertExhibitionPhotos($batch) . ' photos inserted!<br>';
        echo "process images - DONE<br><hr>";
        
	}*/
	
	
	/*public function fixFeed()
	{
	    foreach($this->em->getFeed()->result() as $feed)
	    {
	        $ex = $this->em->getExhibitionByEyeoutId($feed->eyeout_id)->row();
	        $this->em->updateFeed($feed->id, $ex->id);
	        echo "updated feed " . $feed->id . '<br>';
	    }
	}

	
	public function fixFavourites()
	{
	    foreach($this->em->getFavouritesFix()->result() as $fav)
	    {
	        $ex = $this->em->getExhibitionByEyeoutId($feed->eyeout_id)->row();
	        $this->em->updateFeed($feed->id, $ex->id);
	        echo "updated feed " . $feed->id . '<br>';
	    }
	}*/
	

}