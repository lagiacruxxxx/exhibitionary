<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Authentication_model', 'am');
    }

    public function login()
    {
        $this->auth->showLogin();
    }
    
    public function do_login()
    {
        $this->auth->do_login();
    }
    
    public function logout()
    {
        $this->auth->logout();
    }
    
    public function reset_password($string)
    {
        $this->session->set_userdata('language', 'en');
    
        $data['hash'] = $string;
        $data['message'] = $this->session->flashdata('message');
        $this->load->view('exhibitionary/reset', $data);
    }
    
    
    function reset_password_form()
    {
        
        //validate form input
    
        $this->form_validation->set_rules('register_pw', "Password", 'trim|required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('register_pw_conf', "Password Confirmation", 'matches[register_pw]');
    
        $hash = $this->input->post('register_hash');
        $password = $this->input->post('register_pw');
        	
        $pw = $this->auth->__createHash($password);
    
        if ($this->form_validation->run() == true)
        {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', "Password updated");
            $data = array('password' => $pw);
            $this->em->updateUserDataHash($data, $hash);
            redirect('reset_password/'.$hash);
        }
        else
        {
            //display the create user form
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->session->set_flashdata('message', $this->data['message']);
            redirect('reset_password/'.$hash);
        }
    
    
    }
        
    
}

/* End of file authentication.php */
/* Location: ./application/controllers/authentication.php */