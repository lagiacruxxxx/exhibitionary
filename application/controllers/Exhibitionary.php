<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Exhibitionary extends MY_Controller 
{
	public $user;
	
    function __construct()
    {
		parent::__construct();
		
		date_default_timezone_set('Europe/Vienna');
		
		$this->auth->checkLogin();
		
		$this->load->model('Authentication_model');
		$this->user = $this->Authentication_model->getAdmindataByID($this->session->userdata('user_id'))->row();
		$this->load->library('Besc_crud');
    }  
    

	public function index()
	{
        $this->render->__renderBackend('exhibitionary/home', array());
	}
	
	
	public function administrator()
	{
	    $bc = new besc_crud();
	    $bc->table('administrator');
	    $bc->primary_key('id');
	    $bc->title('Administrator');
	     
	    $bc->order_by_field('username');
	    $bc->order_by_direction('asc');
	     
	    $bc->list_columns(array('username', 'firstname', 'lastname', 'email'));
	    $bc->filter_columns(array('username', 'firstname', 'lastname', 'email'));
	     
	    $bc->columns(array
        (
            'username' => array(
                'db_name' => 'username',
                'type' => 'text',
                'display_as' => 'Username',
                'validation' => 'required|is_unique[administrator.username]|max_length[255]',
            ),
    
            'firstname' => array(
                'db_name' => 'firstname',
                'type' => 'text',
                'display_as' => 'Firstname',
                'validation' => 'max_length[255]',
            ),
    
            'lastname' => array(
                'db_name' => 'lastname',
                'type' => 'text',
                'display_as' => 'Lastname',
                'validation' => 'max_length[255]',
            ),
    
            'email' => array(
                'db_name' => 'email',
                'type' => 'text',
                'display_as' => 'E-mail',
                'validation' => 'required|is_unique[administrator.email]|valid_email|max_length[255]',
            ),
             
            'pword' => array(
                'db_name' => 'pword',
                'type' => 'hidden',
                'value' => $this->auth->__createHash('start'),
            ),
        ));
	     
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);
	}
	
	
	public function user()
	{
	    $bc = new besc_crud();
	    $bc->table('user');
	    $bc->primary_key('id');
	    $bc->title('User');
	    
	    $bc->list_columns(array('first_name', 'last_name', 'email', 'role'));
	    $bc->filter_columns(array('first_name', 'last_name', 'email', 'newsletter', 'role'));
	    
	    $bc->unset_add();
	    $newsletterOptions = array(
            array('key' => NEWSLETTER_NO, 'value' => 'NO'),
            array('key' => NEWSLETTER_YES, 'value' => 'YES'),
	    );
	    $activeOptions = array(
	        array('key' => USER_INACTIVE, 'value' => 'INACTIVE'),
	        array('key' => USER_ACTIVE, 'value' => 'ACTIVE'),
	    );
	    
	    
	    
	    $bc->columns(array
	        (
	            'first_name' => array(
	                'db_name' => 'first_name',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	                'validation' => 'max_length[255]',
	            ),
	    
	            'last_name' => array(
	                'db_name' => 'last_name',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	                'validation' => 'max_length[255]',
	            ),
	    
	            'email' => array(
	                'db_name' => 'email',
	                'type' => 'text',
	                'display_as' => 'E-mail',
	                'validation' => 'required|is_unique[administrator.email]|valid_email|max_length[255]',
	            ),
	            
	            'newsletter' => array(
                    'db_name' => 'newsletter',
	                'type' => 'select',
	                'display_as' => 'Newsletter registrations',
	                'options' => $newsletterOptions, 
	            ),
	            
	            'active' => array(
	                'db_name' => 'active',
	                'type' => 'select',
	                'display_as' => 'Active',
	                'options' => $activeOptions,
	            ),
	            
	            'role' => array(
	                'db_name' => 'role',
	                'type' => 'text',
	                'display_as' => 'Role',
	                'validation' => 'max_length[255]',
	            ),
	            
	            'favourites_relation' => array
	            (
	                'relation_id' => 'favourites_relation',
	                'type' => 'm_n_relation',
	                'table_mn' => 'user_favourites',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'user_id',
	                'table_mn_col_n' => 'exhibition_id',
	                'table_m' => 'user',
	                'table_n' => 'exhibition',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'title',
	                'display_as' => 'Favourites',
	                'box_width' => 150,
	                'box_height' => 150,
	            ),
	             
	            
 	            /*'fb_id' => array(
	                'db_name' => 'fb_id',
	                'type' => 'text',
	                'display_as' => 'Facebook ID',
	                'validation' => 'max_length[255]',
	            ),
	            
	            'activation_code' => array(
	                'db_name' => 'activation_code',
	                'type' => 'text',
	                'display_as' => 'E-mail',
	                'validation' => 'max_length[255]',
	            ),
	             
	            'forgotten_password_code' => array(
	                'db_name' => 'forgotten_password_code',
	                'type' => 'text',
	                'display_as' => 'Forgotten password code',
	                'validation' => 'max_length[255]',
	            ),
	            
	            'forgotten_password_time' => array(
	                'db_name' => 'forgotten_password_code',
	                'type' => 'text',
	                'display_as' => 'Forgotten password time',
	                'validation' => 'max_length[255]',
	            ),
	            
	            'remember_code' => array(
	                'db_name' => 'remember_code',
	                'type' => 'text',
	                'display_as' => 'Forgotten password time',
	                'validation' => 'max_length[40]',
	            ),*/	            
	            
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);	    
	}
	
	
	
	public function city()
	{
	    $bc = new besc_crud();
	    $bc->table('city');
	    $bc->primary_key('id');
	    $bc->title('City');
	
	    $bc->order_by_field('name');
	    $bc->order_by_direction('asc');
	
	    $bc->list_columns(array('name', 'visible'));
	    $bc->filter_columns(array('name', 'visible'));
	    
	    $visibleOptions = array(
	        array('key' => EXHIBITION_VISIBLE, 'value' => 'VISIBLE'),
	        array('key' => EXHIBITION_HIDDEN, 'value' => 'HIDDEN'),
	    );
	
	    $bc->columns(array
	        (
	            'name' => array(
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required|max_length[255]',
	            ),
	
	            'gl_lat' => array(
	                'db_name' => 'gl_lat',
	                'type' => 'text',
	                'display_as' => 'Geolocation - Latitude',
	                'validation' => 'max_length[15]',
	            ),
	            
	            'gl_long' => array(
	                'db_name' => 'gl_long',
	                'type' => 'text',
	                'display_as' => 'Geolocation - Longitude',
	                'validation' => 'max_length[15]',
	            ),
	            
	            'visible' => array(
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visible',
	                'options' => $visibleOptions,
	            ),
	             
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);
	}	
	
	

	public function quarter()
	{
	    $bc = new besc_crud();
	    $bc->table('quarter');
	    $bc->primary_key('id');
	    $bc->title('Quarter');
	
	    $bc->order_by_field('name');
	    $bc->order_by_direction('asc');
	
	    $bc->list_columns(array('name', 'city_id'));
	    $bc->filter_columns(array('name', 'city_id'));
	
	    $cityOptions = array();
	    foreach($this->em->getCities()->result() as $key => $value)
	    {
	        $cityOptions[] = array('key' => $value->id, 'value' => $value->name);
	    }	    
	    
	    $bc->columns(array
        (
            'name' => array(
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required|max_length[255]',
            ),

            'city_id' => array(
                'db_name' => 'city_id',
                'type' => 'select',
                'display_as' => 'City',
                'options' => $cityOptions,
            ),

        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);
	}	
	
	
	public function exhibition()
	{
	    $bc = new besc_crud();
	    $bc->table('exhibition');
	    $bc->primary_key('id');
	    $bc->title('Exhibition');
	
	    $bc->list_columns(array('title', 'startdate', 'enddate', 'visible', 'venue_id', 'city_id'));
	    $bc->filter_columns(array('title', 'visible', 'city_id'));
	    
	    $bc->order_by_field('title');
	    $bc->order_by_direction('asc');

	    $visibleOptions = array(
	        array('key' => EXHIBITION_VISIBLE, 'value' => 'VISIBLE'),
	        array('key' => EXHIBITION_HIDDEN, 'value' => 'HIDDEN'),
	    );
	    
	    $venueOptions = array();
	    $venueOptions[] = array('key' => NOVENUE, 'value' => 'NO VENUE');
	    foreach($this->em->getVenues()->result() as $key => $value)
	    {
	        $venueOptions[] = array('key' => $value->id, 'value' => $value->name);
	    }
	    
	    $cityOptions = array();
	    foreach($this->em->getCities()->result() as $key => $value)
	    {
	        $cityOptions[] = array('key' => $value->id, 'value' => $value->name);
	    }
	    
	    $bc->custom_buttons(array(
	        array(
	            'name' => 'Photos',
	            'icon' => site_url('items/exhibitionary/img/icon_photos.png'),
	            'add_pk' => true,
	            'url' => 'exhibition_photos'),
	    ));
	     	    
	    
	    $bc->columns(array
	        (
	            'title' => array(
	                'db_name' => 'title',
	                'type' => 'text',
	                'display_as' => 'Title',
	                'validation' => 'required|max_length[255]',
	            ),
	
	            'description' => array(
	                'db_name' => 'description',
	                'type' => 'ckeditor',
	                'display_as' => 'Description',
	                'height' => 400,
	            ),
	            
	            'venue_id' => array(
	                'db_name' => 'venue_id',
	                'type' => 'combobox',
	                'display_as' => 'Venue',
	                'options' => $venueOptions,
	            ),
	            
	            'startdate' => array
	            (
	                'db_name' => 'startdate',
	                'type' => 'date',
	                'display_as' => 'Start date',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y',
	                'defaultvalue' => date('d.m.Y'),
	            ),
	            
	            'enddate' => array
	            (
	                'db_name' => 'enddate',
	                'type' => 'date',
	                'display_as' => 'End date',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y',
	                'defaultvalue' => date('d.m.Y'),
	            ),
	            
	            'opening_start' => array
	            (
	                'db_name' => 'opening_start',
	                'type' => 'date',
	                'display_as' => 'Opening date',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y',
	                'defaultvalue' => date('d.m.Y'),
	            ),
	            
	            'opening_start_time' => array(
	                'db_name' => 'opening_start_time',
	                'type' => 'text',
	                'display_as' => 'Opening start time',
	                'col_info' => 'In local time (HH:mm)',
	                'validation' => 'max_length[10]',
	            ),
	            
	            'opening_end_time' => array(
	                'db_name' => 'opening_end_time',
	                'type' => 'text',
	                'display_as' => 'Opening end time',
	                'col_info' => 'In local time (HH:mm)',
	                'validation' => 'max_length[10]',
	            ),
	            
	            'city_id' => array(
	                'db_name' => 'city_id',
	                'type' => 'select',
	                'display_as' => 'City',
	                'options' => $cityOptions,
	            ),
	            
	            'visible' => array(
	                'db_name' => 'visible',
	                'type' => 'select',
	                'display_as' => 'Visible',
	                'options' => $visibleOptions,
	            ),
	
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);
	}

	
	public function exhibition_photos($exhibition_id)
	{
	    $data = array();
	    $data['exhibition'] = $this->em->getExhibitionById($exhibition_id)->row();
	    $data['images'] = $this->em->getExhibitionPhotosById($exhibition_id);
	    $this->render->__renderBackend('exhibitionary/exhibition_images', $data);
	}
	
	public function save_exhibition_photos()
	{
	    if($this->input->post('exhibition_id') != null)
	    {
	        $exhibitionId = $this->input->post('exhibition_id');
	        $this->em->deleteExhibitionPhotos($exhibitionId);
	        $batch = array();
	        if($this->input->post('photos') != null)
	        {
    	        foreach($this->input->post('photos') as $item)
    	        {
    	            $batch[] = array(
    	                'exhibition_id' => $exhibitionId,
    	                'fname' => $item['fname'],
    	                'ordering' => $item['ordering'],
    	            );
    	        }
    	    
    	        $this->em->insertExhibitionPhotos($batch);
	        }
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );	    
	}
	
	
	public function venue()
	{
	    $bc = new besc_crud();
	    $bc->table('venue');
	    $bc->primary_key('id');
	    $bc->title('Venue');
	    
	    $bc->order_by_field('name');
	    $bc->order_by_direction('asc');
	
	    $bc->list_columns(array('name', 'city_id', 'visible', 'quarter'));
	    $bc->filter_columns(array('name', 'city_id', 'visible', 'quarter'));

	    $visibleOptions = array(
	        array('key' => VENUE_VISIBLE, 'value' => 'VISIBLE'),
	        array('key' => VENUE_HIDDEN, 'value' => 'HIDDEN'),
	    );
	    
	    $cityOptions = array();
	    foreach($this->em->getCities()->result() as $key => $value)
	    {
	        $cityOptions[] = array('key' => $value->id, 'value' => $value->name);
	    }
	    
	    $quarterOptions = array();
	    foreach($this->em->getQuarters()->result() as $key => $value)
	    {
	        $quarterOptions[] = array('key' => $value->id, 'value' => $value->name);
	    }
	    
	    $bc->columns(array
        (
            'name' => array(
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required|max_length[255]',
            ),
            
            'sort' => array(
                'db_name' => 'sort',
                'type' => 'text',
                'display_as' => 'Sort',
                'validation' => '',
            ),

            'street' => array(
                'db_name' => 'street',
                'type' => 'text',
                'display_as' => 'Street',
                'validation' => 'required|max_length[255]',
            ),
            
            'postcode' => array(
                'db_name' => 'postcode',
                'type' => 'text',
                'display_as' => 'ZIP code',
                'validation' => 'required|max_length[255]',
            ),
            
           
            'city_id' => array(
                'db_name' => 'city_id',
                'type' => 'select',
                'display_as' => 'City',
                'options' => $cityOptions,
            ),
            
            'quarter' => array(
                'db_name' => 'quarter',
                'type' => 'select',
                'display_as' => 'Quarter',
                'options' => $quarterOptions,
            ),

            'phone' => array(
                'db_name' => 'phone',
                'type' => 'text',
                'display_as' => 'Phone',
                'validation' => 'required|max_length[50]',
            ),
            
            'web' => array(
                'db_name' => 'web',
                'type' => 'url',
                'display_as' => 'Website',
                'validation' => 'required|max_length[255]',
            ),
            
            'email' => array(
                'db_name' => 'email',
                'type' => 'text',
                'display_as' => 'E-mail',
                'validation' => 'required|valid_email|max_length[255]',
            ),
            
            'hours' => array(
                'db_name' => 'hours',
                'type' => 'text',
                'display_as' => 'Open hours',
                'validation' => 'required|max_length[255]',
            ),
            
            'gl_lat' => array(
                'db_name' => 'gl_lat',
                'type' => 'text',
                'display_as' => 'Geolocation - Latitude',
                'validation' => 'required|max_length[15]',
            ),
            
            'gl_long' => array(
                'db_name' => 'gl_long',
                'type' => 'text',
                'display_as' => 'Geolocation - Longitude',
                'validation' => 'required|max_length[15]',
            ),
            
            'visible' => array(
                'db_name' => 'visible',
                'type' => 'select',
                'display_as' => 'Visible',
                'options' => $visibleOptions,
            ),

        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);
	}
	
	public function picks($cityId)
	{
	    $bc = new besc_crud();
	    $bc->table('feed');
	    $bc->primary_key('id');
	    $bc->title('Picks');
	     
	    $bc->order_by_field('exhibition_id');
	    $bc->order_by_direction('asc');
	    
	    $bc->where('city_id = ' . $cityId);
	    
	    $bc->list_columns(array('name', 'city_id', 'exhibition_id'));
	    
	    $exOptions = array();
	    foreach($this->em->getExhibitionsByCity($cityId)->result() as $key => $value)
	    {
	        $exOptions[] = array('key' => $value->id, 'value' => $value->title);
	    }
	     
	     
	    $bc->columns(array
	        (
	            'city_id' => array(
	                'db_name' => 'city_id',
	                'type' => 'hidden',
	                'value' => $cityId,
	            ),
	    
	            'exhibition_id' => array(
	                'db_name' => 'exhibition_id',
	                'type' => 'combobox',
	                'display_as' => 'Exhibition',
	                'validation' => 'required',
	                'options' => $exOptions,
	            ),
	    
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->render->__renderBackend('exhibitionary/crud', $data);	    
	}
	
}