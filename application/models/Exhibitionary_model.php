<?php

class Exhibitionary_model extends CI_Model
{

    function getVenues()
    {
        return $this->db->get('venue');
    }
    
    function updateVenue($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('venue', $data);
    }
    
    function getCities()
    {
        return $this->db->get('city');
    }
    
    function getVisibleCities()
    {
        $this->db->where('visible', EXHIBITION_VISIBLE);
        return $this->db->get('city');
    }
    
    function getQuarters()
    {
        return $this->db->get('quarter');
    }
    
    function getExhibitionsByCity($cityId)
    {
        $this->db->where('city_id', $cityId);
        $this->db->where('visible', 1);
        return $this->db->get('exhibition');
    }
    
    function getExhibitionById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('exhibition');
    }
    
    function getExhibitionPhotosById($id)
    {
        $this->db->where('exhibition_id', $id);
        $this->db->order_by('ordering');
        return $this->db->get('exhibition_photos');
    }
    
    function deleteExhibitionPhotos($exhibitionId)
    {
        $this->db->where('exhibition_id', $exhibitionId);
        return $this->db->delete('exhibition_photos');
    }
    
    function insertExhibitionPhotos($batch)
    {
        return $this->db->insert_batch('exhibition_photos', $batch);
    }
    
    function updateUserDataHash($data, $hash)
    {
        $this->db->where('forgotten_password_code', $hash);
        $this->db->update('user', $data);
    }
    
    function updateUserData($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    function getUser($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('user');
    }
    
    function checkMobilePW($email){
        $sql = "SELECT *  FROM  user  WHERE  `email` = ?";
        $result = $this->db->query($sql, array($email));
        if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function checkLoginUserFB($email){
        $sql = "SELECT *  FROM  user  WHERE  `email` = ?";
        $result = $this->db->query($sql, array($email));
        if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function checkFbId($fbId){
        $sql = "SELECT *  FROM  user  WHERE  `fb_id` = ?";
        $result = $this->db->query($sql, array($fbId));
        if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function checkFbToEmail($fbId, $email){
        $sql = "SELECT *  FROM  user  WHERE  `fb_id` = ? AND `email` = ?";
        $result = $this->db->query($sql, array($fbId, $email));
        if ($result->num_rows() > 0) {return true;}else {return false;}
    }
    
    function saveSession($data){
        $this->db->insert('sessions', $data);
        return $this->db->insert_id();
    }
    
    function updateProfilePic($email, $data)
    {
        $this->db->where('email', $email);
        $this->db->update('user', $data);
    }
    
    function checkSession($session_id)
    {
        $sql = 'SELECT * FROM sessions WHERE session_id = ?';
        $resultset = $this->db->query($sql, array($session_id));
    
        if ($resultset->num_rows() > 0) {return $resultset->row();}else {return "NO";}
    }
    
    function checkSessionUser($session_id, $user_id)
    {
        $sql = 'SELECT * FROM sessions WHERE session_id = ? AND user_id = ?';
        $resultset = $this->db->query($sql, array($session_id, $user_id));
        if ($resultset->num_rows() > 0) {return $resultset->row();}else {return "NO";}
    }
    
    function updateSession($session_id)
    {
        $this->db->where('session_id', $session_id);
        $this->db->update('sessions', array('session_id' => $session_id));
    }
    
    
    function sessionData($session_id)
    {
        $sql = 'SELECT * FROM sessions WHERE session_id = ?';
        $resultset = $this->db->query($sql, array($session_id));
    
        if ($resultset->num_rows() > 0) {return $resultset->row();}else {return false;}
    }
    
    function getFavourites($id)
    {
        $this->db->where('user_id', $id);
        $this->db->where('exhibition_id != ', 0);
        return $this->db->get('user_favourites');
    
    }
    
    function addFavourite($data)
    {
        $this->db->insert('user_favourites', $data);
        return $this->db->insert_id();
    }
    
    function deleteFavourite($id,$eye_id)
    {
        $this->db->where('user_id', $id);
        $this->db->where('exhibition_id', $eye_id);
        $this->db->delete('user_favourites');
    }
    
    function checkFavourite($id,$eid)
    {
        $this->db->where('user_id', $id);
        $this->db->where('exhibition_id', $eid);
        $result = $this->db->get('user_favourites');
        return ( $result->num_rows() > 0 ) ? true : false;
    }
    
    function getCityFeed($id)
    {
        $this->db->where('city_id', $id);
        $this->db->where('exhibition_id !=', 0);
        $this->db->order_by('id', 'desc');
        return $this->db->get('feed');
    }
    
    function getContributorFeed($id)
    {
        $this->db->where('contributor_id', $id);
        return $this->db->get('contributor_list');
    
    }
    

    function addStat($data)
    {
        $this->db->insert('page_statistics', $data);
        return $this->db->insert_id();
    }
    
    
    function getAPIExhibitionById($id)
    {
        $this->db->select('exhibition.*, city.name as city_name');
        $this->db->where('exhibition.id', $id);
        $this->db->join('city', 'city.id = exhibition.city_id');
        return $this->db->get('exhibition');
    }
    
    function getAPIVenueById($id)
    {
        $this->db->select('venue.*, city.name as city_name, quarter.name as quarter_name');
        $this->db->where('venue.id', $id);
        $this->db->where('venue.visible', 1);
        $this->db->join('city', 'city.id = venue.city_id');
        $this->db->join('quarter', 'quarter.id = venue.quarter');
        return $this->db->get('venue');
    }
    
    function getAPIgetExhibitions($city_id, $limit, $offset)
    {
        $this->db->select('exhibition.*');
        $this->db->where('exhibition.city_id', $city_id);
        $this->db->where('enddate >', date('Y-m-d'));
        $this->db->limit($limit, $offset);
        $this->db->join('venue', 'venue.id = exhibition.venue_id');
        $this->db->order_by('venue.sort', 'asc');
        $this->db->order_by('exhibition.startdate', 'asc');
        return $this->db->get('exhibition');
    }
    
    function getAPIgetFutureExhibitions($city_id, $limit, $offset)
    {
        $this->db->select('exhibition.*');
        $this->db->where('exhibition.city_id', $city_id);
        $this->db->where('enddate >=', date('Y-m-d'));
        $this->db->where('startdate >=', date('Y-m-d'));
        $this->db->limit($limit, $offset);
        $this->db->join('venue', 'venue.id = exhibition.venue_id');
        $this->db->order_by('venue.sort', 'asc');
        $this->db->order_by('exhibition.startdate', 'asc');
        return $this->db->get('exhibition');
    }
    
    function getAPIExhibtionPhotos($exhibition_id)
    {
        $this->db->where('exhibition_id', $exhibition_id);
        return $this->db->get('exhibition_photos');
    }
    
    function getAPIgetExhibitionsArray($array, $limit, $offset)
    {
        $this->db->where_in('id', $array);
        $this->db->limit($limit, $offset);
        $this->db->where('enddate >', date('Y-m-d'));
        return $this->db->get('exhibition');        
    }
    
    
    
    
    function deleteVenues($cityId)
    {
        $this->db->where('city_id', $cityId);
        $this->db->delete('venue');
    }
    
    function deleteQuarters($cityId)
    {
        $this->db->where('city_id', $cityId);
        $this->db->delete('quarter');
    }
    
    function deleteExhibitions($cityId)
    {
        $this->db->where('city_id', $cityId);
        $this->db->delete('exhibition');
    }
    
    function insertQuarters($batch)
    {
        return $this->db->insert_batch('quarter', $batch);
    }
    
    function insertVenues($batch)
    {
        return $this->db->insert_batch('venue', $batch);
    }
    
    function insertExhibitions($batch)
    {
        return $this->db->insert_batch('exhibition', $batch);
    }
    
    function xrefExhibitions($xref)
    {
        $this->db->where_not_in('eyeout_id', $xref);
        $this->db->delete('exhibition');
        
    }
    
    function xrefVenues($xref)
    {
        $this->db->where_not_in('eyeout_id', $xref);
        $this->db->delete('venue');
    
    }
    
    function getVenueByEyeoutId($eyeoutId)
    {
        $this->db->where('eyeout_id', $eyeoutId);
        return $this->db->get('venue');
    }
    
    function getMaxExhibitionId()
    {
        $this->db->select_max('id');
        return $this->db->get('exhibition');
    }
    
    function getExhibitionByEyeoutId($eyeoutId)
    {
        $this->db->where('eyeout_id', $eyeoutId);
        return $this->db->get('exhibition');
    }
    
    
    function getFeed()
    {
        return $this->db->get('feed');
    }
    
    function updateFeed($feedId, $exId)
    {
        $this->db->where('id', $feedId);
        $this->db->set('exhibition_id', $exId);
        $this->db->update('feed');
    }
    
    function getFavouritesFix($id)
    {
        return $this->db->get('user_favourites');
    }

}

?>