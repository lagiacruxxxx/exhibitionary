<?php

class Authentication_model extends CI_Model
{
    function getAdminPW($username)
    {
        $this->db->where('username', $username);
        $this->db->select('pword');
        return $this->db->get('administrator');
    }

    function getAdmindataByUsername($username)
    {
        $this->db->where('username', $username);
        return $this->db->get('administrator');
    }

    function getAdmindataByID($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('administrator');
    }
    
    function updateUser($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('administrator', $data);
    }
    
    
    
    /*function getUserPW($username)
    {
        $this->db->where('username', $username);
        $this->db->select('pword');
        return $this->db->get('administrator');
    }
    
    function getUserdataByUsername($username)
    {
        $this->db->where('username', $username);
        return $this->db->get('administrator');
    }
    
    function getUserdataByID($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('administrator');
    }*/
    
}

?>