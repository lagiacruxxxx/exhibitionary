<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    public $language = NULL;
    protected $is_mobile = NULL;
    protected $is_ipad = NULL;
    protected $auth = NULL;
    public $render = null;
    	
    function __construct()
    {
        parent::__construct();

        $this->_connectDB();
        $this->_setDocumentRoot();
        $this->load->library('session');
        date_default_timezone_set('Europe/Vienna');
        
        $this->config->load('exhibitionary');
        $this->config->load('ion_auth');
        $this->load->helper('besc_helper');
        
        require_once (APPPATH . 'libraries/MyRender.php');
        require_once (APPPATH . 'libraries/MyAuth.php');
        
        $this->render = new MyRender();
        $this->auth = new MyAuth();
    }  

    private function _connectDB()
    {
        /*if( (strpos(site_url(), '127.0.0.1') !== false || strpos(site_url(), 'localhost') !== false) || (strpos(site_url(), '192.168.1') !== false && strpos(site_url(), '192.168.0.7') === false) )
            $this->load->database('development');
        elseif(strpos(site_url(), 'fileserver') !== false || strpos(site_url(), '192.168.0.7') !== false)
            $this->load->database('testing');
        else*/
            $this->load->database('productive');
    }

    private function _setDocumentRoot()
    {
        if( (strpos(site_url(), '127.0.0.1') !== false || strpos(site_url(), 'localhost') !== false) || (strpos(site_url(), '192.168.1') !== false && strpos(site_url(), '192.168.0.7') === false) )
            $this->documentRoot = '/frink';
        elseif(strpos(site_url(), 'fileserver') !== false || strpos(site_url(), '192.168.0.7') !== false)
            $this->documentRoot = '/frinkacademy';
        else
            $this->documentRoot = '';
    }
    
    protected function logged_in()
    {
        return (bool) $this->session->userdata('user_id');
    }

}
